//
//  RouterTests.swift
//  NotesUnitTests
//
//  Created by Anton Bugera on 30.10.2021.
//

import XCTest
@testable import Notes

class RouterTests: XCTestCase {
    
    var viewTransition: ViewTransitionMock!
    var navigationController: NavigationControllerMock!
    var router: Router!

    override func setUpWithError() throws {
        viewTransition = .init()
        navigationController = .init()
        router = .init(rootViewController: navigationController, viewTranslationable: viewTransition)
    }

    override func tearDownWithError() throws {
        navigationController = nil
        router = nil
    }

    func testPushed() {
        // Arrange
        let viewController = UIViewController()
        // Act
        router.push(viewController)
        // Assert
        XCTAssertIdentical(viewController, navigationController.pushedViewController)
    }
    
    func testSetRoot() {
        // Arrange
        let viewController = UIViewController()
        // Act
        router.setRootController(viewController)
        // Assert
        XCTAssertNotNil(viewTransition.transitionedView)
    }
   
}
