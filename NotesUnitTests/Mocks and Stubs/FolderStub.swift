//
//  FolderStub.swift
//  NotesUnitTests
//
//  Created by Anton Bugera on 24.10.2021.
//

import Foundation
@testable import Notes

class ObjectIDStub: ObjectID {
    
}

class FolderStub: FolderProtocol {
    var creationDate: Date?
    var name: String?
    var notes: NSOrderedSet?
    
    var contentObjectID: ObjectID = ObjectIDStub()
    
}
