//
//  NoteDetailsCoordinatorMock.swift
//  NotesUnitTests
//
//  Created by Anton Bugera on 30.10.2021.
//

import Foundation
@testable import Notes

class NoteDetailsCoordinatorMock: NoteDetailsCoordinatorProtocol {
    var isStarted = false
    var startedWithNote: NoteProtocol?
    
    func start() {
        isStarted = true
    }
    
    func start(_ note: NoteProtocol) {
        startedWithNote = note
    }
}
