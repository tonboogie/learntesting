//
//  DatabaseMock.swift
//  NotesUnitTests
//
//  Created by Anton Bugera on 24.10.2021.
//

import Foundation
@testable import Notes

final class DatabaseMock: DatabaseProtocol {
    var openCalled = false
    var createdFolderName: String?
    var deletedFolder: FolderProtocol!
    var deletedNote: NoteProtocol?
    var createdNoteName: String?
    
    func open(completion: @escaping () -> Void) {
        openCalled = true
    }
    
    func create(folderName name: String, creationDate: Date, completion: @escaping (Error?) -> Void) {
        createdFolderName = name
    }
    
    func create(noteName name: String, body: String, folderId: ObjectID, creationDate: Date, completion: @escaping (Error?) -> Void) {
        createdNoteName = name
    }
    
    func delete(folder: FolderProtocol) {
        deletedFolder = folder
    }
    
    func delete(note: NoteProtocol) {
        deletedNote = note
    }
}
