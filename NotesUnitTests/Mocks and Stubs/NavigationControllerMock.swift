//
//  NavigationControllerMock.swift
//  NotesUnitTests
//
//  Created by Anton Bugera on 30.10.2021.
//

import UIKit
@testable import Notes

class NavigationControllerMock: NavigationController {
    
    var view: UIView! = UIView()
    var viewControllers: [UIViewController]?
    var pushedViewController: UIViewController?
    
    func setViewControllers(_ viewControllers: [UIViewController], animated: Bool) {
        self.viewControllers = viewControllers
    }
    
    func pushViewController(_ viewController: UIViewController, animated: Bool) {
        pushedViewController = viewController
    }
    
}
