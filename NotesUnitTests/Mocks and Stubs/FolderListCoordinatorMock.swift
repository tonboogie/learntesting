//
//  FolderListCoordinatorMock.swift
//  NotesUnitTests
//
//  Created by Anton Bugera on 24.10.2021.
//

import Foundation
@testable import Notes

class FolderListCoordinatorMock: FolderListCoordinatorProtocol {
    var receivedObjectID: ObjectID?
    
    func showNotesList(folderId: ObjectID) {
        receivedObjectID = folderId
    }
}
