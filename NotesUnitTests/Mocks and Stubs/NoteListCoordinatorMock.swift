//
//  NoteListCoordinatorMock.swift
//  NotesUnitTests
//
//  Created by Anton Bugera on 27.10.2021.
//

import Foundation
@testable import Notes

class NoteListCoordinatorMock: NoteListCoordinatorProtocol {
    var isStarted = false
    var isShowNoteCreationCalled = false
    var noteToBeShownDetails: NoteProtocol!
    
    func start() {
        isStarted = true
    }
    
    func showNoteCreation() {
        isShowNoteCreationCalled = true
    }
    
    func showNoteDetails(_ note: NoteProtocol) {
        noteToBeShownDetails = note
    }
}
