//
//  ViewTransitionMock.swift
//  NotesUnitTests
//
//  Created by Anton Bugera on 30.10.2021.
//

import UIKit
@testable import Notes

class ViewTransitionMock: ViewTransitionable {
    var transitionedView: UIView?
    
    func transition(with view: UIView,
                    duration: TimeInterval,
                    options: UIView.AnimationOptions,
                    animations: (() -> Void)?,
                    completion: ((Bool) -> Void)?) {
        transitionedView = view
    }
}
