//
//  DataSourceMock.swift
//  NotesUnitTests
//
//  Created by Anton Bugera on 27.10.2021.
//

import Foundation
@testable import Notes

class DataSourceMock: DataSourceProtocol {
    var delegate: DataSourceDelegate?
    var sections: [DataSourceSectionInfo]?
    var sortedByCondition: SortCondition?
    
    func performFetch() throws {
        
    }
    
    func object(at: IndexPath) -> Note {
        return Note()
    }
    
    func sort(by sort: SortCondition) throws {
        sortedByCondition = sort
    }
}
