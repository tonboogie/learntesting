//
//  RouterMock.swift
//  NotesUnitTests
//
//  Created by Anton Bugera on 30.10.2021.
//

import UIKit
@testable import Notes

class RouterMock: RouterProtocol {
    
    var pushedViewController: UIViewController?
    
    func push(_ viewController: UIViewController) {
        pushedViewController = viewController
    }
    
    func setRootController(_ viewController: UIViewController) {
        
    }
}
