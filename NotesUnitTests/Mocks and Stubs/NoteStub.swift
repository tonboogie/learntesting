//
//  NoteStub.swift
//  NotesUnitTests
//
//  Created by Anton Bugera on 28.10.2021.
//

import Foundation
@testable import Notes

class NoteStub: NoteProtocol {
    var body: String?
    var creationDate: Date?
    var name: String?
    var folder: Folder?

    var contentObjectID: ObjectID = ObjectIDStub()
    
    func update(name: String, body: String) {
        self.name = name
        self.body = body
    }
}
