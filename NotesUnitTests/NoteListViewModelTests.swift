//
//  NoteListViewModelTests.swift
//  NotesUnitTests
//
//  Created by Anton Bugera on 24.10.2021.
//

import XCTest
@testable import Notes

class NoteListViewModelTests: XCTestCase {
    
    var database: DatabaseMock!
    var dataSource: DataSourceMock!
    var coordinator: NoteListCoordinatorMock!
    var viewModel: NoteListViewModel!
    
    override func setUpWithError() throws {
        database = .init()
        dataSource = .init()
        coordinator = .init()
        viewModel = .init(coordinator: coordinator,
                          folderId: ObjectIDStub(),
                          dataSource: dataSource,
                          database: database)
    }
    
    override func tearDownWithError() throws {
        database = nil
        dataSource = nil
        coordinator = nil
        viewModel = nil
    }
    
    func testShowNoteCreationCalled() {
        // Arrange
        // Act
        viewModel.addNoteTapped()
        // Assert
        XCTAssertTrue(coordinator.isShowNoteCreationCalled)
    }
    
    func testNoteCanBeDeleted() {
        // Arrange
        let note = NoteStub()
        // Act
        viewModel.delete(note: note)
        // Assert
        XCTAssertIdentical(note, database.deletedNote)
    }
    
    func testShowNoteDetailsCalled() {
        // Arrange
        let note = NoteStub()
        // Act
        viewModel.tappedNote(note: note)
        // Assert
        XCTAssertIdentical(note, coordinator.noteToBeShownDetails)
    }
    
    func testSortedByCondition() throws {
        // Act
        viewModel.sort(by: .name)
        // Assert
        XCTAssertEqual(dataSource.sortedByCondition, .name)
    }
}
