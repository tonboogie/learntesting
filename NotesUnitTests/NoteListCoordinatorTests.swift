//
//  NoteListCoordinatorTests.swift
//  NotesUnitTests
//
//  Created by Anton Bugera on 30.10.2021.
//

import XCTest
@testable import Notes

class NoteListCoordinatorTests: XCTestCase {

    var folder: FolderStub!
    var router: RouterMock!
    var database: DatabaseMock!
    var datasource: DataSourceMock!
    var coordinator: NoteListCoordinator!
    var noteDetailsCoordinator: NoteDetailsCoordinatorMock!
        
    override func setUpWithError() throws {
        folder = .init()
        router = .init()
        database = .init()
        datasource = .init()
        noteDetailsCoordinator = .init()
        coordinator = .init(router: router,
                            folderId: folder.contentObjectID,
                            dataBase: database,
                            dataSource: datasource,
                            noteDetailsCoordinator: noteDetailsCoordinator)
    }

    override func tearDownWithError() throws {
        folder = nil
        router = nil
        database = nil
        datasource = nil
        noteDetailsCoordinator = nil
        coordinator = nil
    }

    func testStarted() {
        // Arrange
        // Act
        coordinator.start()
        // Assert
        XCTAssertNotNil(router.pushedViewController)
    }
    
    func testNoteCreation() {
        // Arrange
        // Act
        coordinator.showNoteCreation()
        // Assert
        XCTAssertTrue(noteDetailsCoordinator.isStarted)
    }
    
    func testNoteDetails() {
        // Arrange
        let note = NoteStub()
        // Act
        coordinator.showNoteDetails(note)
        // Assert
        XCTAssertIdentical(note, noteDetailsCoordinator.startedWithNote)
    }

}
