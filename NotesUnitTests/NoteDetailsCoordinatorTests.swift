//
//  NoteDetailsCoordinatorTests.swift
//  NotesUnitTests
//
//  Created by Anton Bugera on 30.10.2021.
//

import XCTest
@testable import Notes

class NoteDetailsCoordinatorTests: XCTestCase {
    
    var objectId: ObjectIDStub!
    var router: RouterMock!
    var database: DatabaseMock!
    var coordinator: NoteDetailsCoordinator!

    override func setUpWithError() throws {
        objectId = .init()
        router = .init()
        database = .init()
        coordinator = .init(router: router, folderId: objectId, dataBase: database)
    }

    override func tearDownWithError() throws {
        objectId = nil
        router = nil
        database = nil
        coordinator = nil
    }

    func testStarted() {
        // Arrange
        // Act
        coordinator.start()
        // Assert
        XCTAssertNotNil(router.pushedViewController)
    }
    
    func testStartedWithNote() {
        // Arrange
        let note = NoteStub()
        // Act
        coordinator.start(note)
        // Assert
        XCTAssertNotNil(router.pushedViewController)
    }

}
