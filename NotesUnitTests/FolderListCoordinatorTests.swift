//
//  FolderListCoordinatorTests.swift
//  NotesUnitTests
//
//  Created by Anton Bugera on 29.10.2021.
//

import XCTest
@testable import Notes

class FolderListCoordinatorTests: XCTestCase {
    
    var router: RouterMock!
    var database: DatabaseMock!
    var coordinator: FolderListCoordinator!
    var noteListCoordinatorMock: NoteListCoordinatorMock!

    override func setUpWithError() throws {
        router = .init()
        database = .init()
        noteListCoordinatorMock = .init()
        coordinator = .init(router: router,
                            database: database,
                            noteListCoordinator: noteListCoordinatorMock)
    }

    override func tearDownWithError() throws {
        router = nil
        database = nil
        noteListCoordinatorMock = nil
        coordinator = nil
    }
    
    func testStarted() {
        // Arrange
        // Act
        coordinator.start()
        // Assert
        XCTAssertTrue(database.openCalled)
    }
    
    func testOpenNoteList() {
        // Arrange
        let folder = FolderStub()
        // Act
        coordinator.showNotesList(folderId: folder.contentObjectID)
        // Assert
        XCTAssertTrue(noteListCoordinatorMock.isStarted)
    }

}
