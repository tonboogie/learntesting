//
//  NoteDetailsViewModelTests.swift
//  NotesUnitTests
//
//  Created by Anton Bugera on 29.10.2021.
//

import XCTest
@testable import Notes

class NoteDetailsViewModelTests: XCTestCase {
    
    var viewModel: NoteDetailsViewModel!
    var database: DatabaseMock!
    
    lazy private var note: NoteStub = {
        let note = NoteStub()
        note.body = "Lorem ipsum..."
        note.creationDate = Date(timeIntervalSince1970: .zero)
        return note
    }()

    override func setUpWithError() throws {
        let id: ObjectID = ObjectIDStub()
        database = .init()
        viewModel = .init(folderId: id, database: database)
    }

    override func tearDownWithError() throws {
        database = nil
        viewModel = nil
    }
    
    func testDisplaysNote() {
        // Arrange
        viewModel.note = note
        // Act
        // Assert
        XCTAssertEqual(note.creationDate?.shortDate(), viewModel.creationDateTitle)
        XCTAssertEqual(note.body, viewModel.body)
    }
    
    func testCreatesNote() {
        // Arrange
        let updatedBody = "... dolor sit amet"
        // Act
        viewModel.update(body: updatedBody)
        // Assert
        XCTAssertEqual(database.createdNoteName, updatedBody)
    }
    
    func testCreateNoteWithLimitedNameLength() {
        // Arrange
        let limitNum = 20
        let longText = String(Array(repeating: "a", count: 21))
        print(longText)
        // Act
        viewModel.update(body: longText)
        // Assert
        XCTAssertEqual(database.createdNoteName?.count, limitNum)
    }
    
    func testUpdatedIfNotEmpty() {
        // Arrange
        viewModel.note = note
        let updatedBody = "... dolor sit amet"
        // Act
        viewModel.update(body: updatedBody)
        // Assert
        XCTAssertEqual(viewModel.body, updatedBody)
    }
    
    func testNotUpdatedIfEmpty() {
        // Arrange
        viewModel.note = note
        let updatedBody = ""
        // Act
        viewModel.update(body: updatedBody)
        // Assert
        XCTAssertNotEqual(viewModel.body, updatedBody)
    }
    
}


