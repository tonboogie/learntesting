//
//  NotesUnitTests.swift
//  NotesUnitTests
//
//  Created by Anton Bugera on 24.10.2021.
//

import XCTest
@testable import Notes

class FolderListViewModelTest: XCTestCase {
    
    var coordinator: FolderListCoordinatorMock!
    var database: DatabaseMock!
    var viewModel: FolderListViewModel!
    
    override func setUpWithError() throws {
        coordinator = .init()
        database = .init()
        viewModel = .init(coordinator: coordinator, database: database)
    }
    
    override func tearDownWithError() throws {
        coordinator = nil
        viewModel = nil
    }
    
    func testFolderIsCreatedWhenNameHasCharacters() {
        // Arrange
        let folderName = "Test Folder"
        // Act
        viewModel.createFolder(name: folderName)
        // Assert
        XCTAssertEqual(database.createdFolderName, folderName)
    }
    
    func testFolderIsNotCreatedWhenNameIsEmpty() {
        // Arrange
        let folderName = ""
        // Act
        viewModel.createFolder(name: folderName)
        // Assert
        XCTAssertNil(database.createdFolderName)
    }
    
    func testNotesListWasDisplayed() {
        // Arrange
        let folder = FolderStub()
        // Act
        viewModel.showNotesList(folder: folder)
        // Assert
        XCTAssertIdentical(folder.contentObjectID, coordinator.receivedObjectID)
    }
    
    func testFolderDeleted() {
        // Arrange
        let folder = FolderStub()
        // Act
        viewModel.delete(folder: folder)
        // Assert
        XCTAssertIdentical(folder, database.deletedFolder)
    }
}
