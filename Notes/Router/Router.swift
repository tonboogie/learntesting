//
//  Router.swift
//  Notes
//
//

import UIKit

protocol ViewTransitionable {
    func transition(with view: UIView,
                    duration: TimeInterval,
                    options: UIView.AnimationOptions,
                    animations: (() -> Void)?,
                    completion: ((Bool) -> Void)?)
}

// MARK: -
protocol NavigationController {
    var view: UIView! { get set }
    func setViewControllers(_ viewControllers: [UIViewController], animated: Bool)
    func pushViewController(_ viewController: UIViewController, animated: Bool)
}

extension UINavigationController: NavigationController {}

// MARK: -
protocol RouterProtocol {
    func push(_ viewController: UIViewController)
    func setRootController(_ viewController: UIViewController)
}
// MARK: -
final class Router: RouterProtocol {
    private let rootViewController: NavigationController
    private let resolvedViewTranslationable: ViewTransitionable?
    
    init(rootViewController: NavigationController, viewTranslationable: ViewTransitionable? = nil) {
        self.rootViewController = rootViewController
        self.resolvedViewTranslationable = viewTranslationable
    }
    
    func push(_ viewController: UIViewController) {
        rootViewController.pushViewController(viewController, animated: true)
    }
    
    func setRootController(_ viewController: UIViewController) {
        guard let resolvedViewTranslationable = resolvedViewTranslationable else {
            UIView.transition(with: rootViewController.view,
                              duration: 0.3,
                              options: .transitionCrossDissolve,
                              animations: { [weak self] in
                                self?.rootViewController.setViewControllers([viewController], animated: false)
                              })
            return
        }
        resolvedViewTranslationable.transition(with: rootViewController.view,
                                               duration: 0.3,
                                               options: .transitionCrossDissolve,
                                               animations: { [weak self] in
                                                self?.rootViewController.setViewControllers([viewController], animated: false)
                                               }, completion: nil)
        
    }
}
