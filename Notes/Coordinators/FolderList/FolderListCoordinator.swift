//
//  FoldersCoordintor.swift
//  Notes
//
//

import UIKit
import CoreData

protocol FolderListCoordinatorProtocol {
    func showNotesList(folderId: ObjectID)
}

class FolderListCoordinator: Coordinator, FolderListCoordinatorProtocol {
    private let router: RouterProtocol
    private let database: DatabaseProtocol
    private let resolvedNoteListCoordinator: NoteListCoordinatorProtocol?
    
    init(router: RouterProtocol, database: DatabaseProtocol, noteListCoordinator: NoteListCoordinatorProtocol? = nil) {
        self.router = router
        self.database = database
        self.resolvedNoteListCoordinator = noteListCoordinator
    }
    
    func start() {
        database.open {
            let folderController: FolderListViewController = FolderListViewController.instantiate()
            folderController.viewModel = FolderListViewModel(coordinator: self, database: self.database)
            
            self.router.setRootController(folderController)
        }
    }
    
    func showNotesList(folderId: ObjectID) {
        guard let noteListCoordinator = resolvedNoteListCoordinator else {
            NoteListCoordinator(router: router,
                                folderId: folderId,
                                dataBase: database).start()
            return
        }
        noteListCoordinator.start()
    }
}
