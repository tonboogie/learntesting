//
//  FolderViewModel.swift
//  Notes
//
//

import UIKit
import CoreData

class FolderListViewModel {
    private let coordinator: FolderListCoordinatorProtocol
    private var sort: SortCondition = .creationDate
    private let database: DatabaseProtocol
    
    var fetchedResultsController: NSFetchedResultsController<Folder>
    
    internal init(coordinator: FolderListCoordinatorProtocol, database: DatabaseProtocol) {
        self.coordinator = coordinator
        self.database = database
        self.fetchedResultsController = Folder.createFetchedResultsController(sort: sort)
        try? self.fetchedResultsController.performFetch()
    }

    func createFolder(name: String) {
        guard !name.isEmpty else { return }
        
        database.create(folderName: name, creationDate: Date(), completion: {_ in })
    }
    
    func delete(folder: FolderProtocol) {
        database.delete(folder: folder)
        
    }

    func showNotesList(folder: FolderProtocol) {
        coordinator.showNotesList(folderId: folder.contentObjectID)
    }
    
    func sort(by sort: SortCondition) {
        guard sort != self.sort else { return }
        self.sort = sort
        
        let delegate = self.fetchedResultsController.delegate
        
        fetchedResultsController = Folder.createFetchedResultsController(sort: sort)
        fetchedResultsController.delegate = delegate
        
        try? self.fetchedResultsController.performFetch()
    }
}
