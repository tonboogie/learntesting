//
//  NotesCoordinator.swift
//  Notes
//
//

import UIKit
import CoreData

// MARK: -
protocol NoteListCoordinatorProtocol {
    func start()
    func showNoteCreation()
    func showNoteDetails(_ note: NoteProtocol)
}

// MARK: -
class NoteListCoordinator: Coordinator, NoteListCoordinatorProtocol {
    private let router: RouterProtocol
    private var folderId: ObjectID
    private let dataSource: DataSourceProtocol
    private let dataBase: DatabaseProtocol
    private let resolvedNoteDetailsCoordinator: NoteDetailsCoordinatorProtocol?
    
    init(router: RouterProtocol,
         folderId: ObjectID,
         dataBase: DatabaseProtocol,
         dataSource: DataSourceProtocol? = nil,
         noteDetailsCoordinator: NoteDetailsCoordinatorProtocol? = nil) {
        self.router = router
        self.folderId = folderId
        self.dataBase = dataBase
        self.resolvedNoteDetailsCoordinator = noteDetailsCoordinator
        if let dataSource = dataSource {
            self.dataSource = dataSource
            return
        }
        self.dataSource = DataSource(sort: .creationDate, folderId: folderId)
    }
    
    func start() {
        let notesController: NoteListViewController = NoteListViewController.instantiate()
        notesController.viewModel = NoteListViewModel(coordinator: self,
                                                      folderId: folderId,
                                                      dataSource: dataSource,
                                                      database: dataBase)
        router.push(notesController)
    }
    
    func showNoteCreation() {
        guard let resolvedNoteDetailsCoordinator = resolvedNoteDetailsCoordinator else {
            NoteDetailsCoordinator(router: router, folderId: folderId, dataBase: dataBase).start()
            return
        }
        resolvedNoteDetailsCoordinator.start()
    }
    
    func showNoteDetails(_ note: NoteProtocol) {
        guard let resolvedNoteDetailsCoordinator = resolvedNoteDetailsCoordinator else {
            NoteDetailsCoordinator(router: router, folderId: folderId, dataBase: dataBase).start(note)
            return
        }
        resolvedNoteDetailsCoordinator.start(note)
    }
}
