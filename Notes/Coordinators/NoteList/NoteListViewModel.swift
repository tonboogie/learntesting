//
//  NotesViewModel.swift
//  Notes
//
//

import UIKit
import CoreData

class NoteListViewModel {
    private let folderId: ObjectID
    private let coordinator: NoteListCoordinatorProtocol
    private var sort: SortCondition = .creationDate
    private let database: DatabaseProtocol
    
    let dataSource: DataSourceProtocol
    
    init(coordinator: NoteListCoordinatorProtocol,
         folderId: ObjectID,
         dataSource: DataSourceProtocol,
         database: DatabaseProtocol) {
        self.coordinator = coordinator
        self.folderId = folderId
        self.dataSource = dataSource
        self.database = database
        try? self.dataSource.performFetch()
    }

    func addNoteTapped() {
        coordinator.showNoteCreation()
    }
    
    func delete(note: NoteProtocol) {
        database.delete(note: note)
    }
    
    func tappedNote(note: NoteProtocol) {
        coordinator.showNoteDetails(note)
    }
    
    func sort(by sort: SortCondition) {
        guard sort != self.sort else { return }
        self.sort = sort
      
        try? dataSource.sort(by: sort)
    }
}
