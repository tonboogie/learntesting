//
//  NoteCoordinator.swift
//  Notes
//
//

import UIKit
import CoreData

// MARK: -
protocol NoteDetailsCoordinatorProtocol {
    func start()
    func start(_ note: NoteProtocol)
}

// MARK: -
class NoteDetailsCoordinator: Coordinator, NoteDetailsCoordinatorProtocol {
    private let router: RouterProtocol
    private let folderId: ObjectID
    private let dataBase: DatabaseProtocol
    
    init(router: RouterProtocol, folderId: ObjectID, dataBase: DatabaseProtocol) {
        self.router = router
        self.folderId = folderId
        self.dataBase = dataBase
    }
    
    func start() {
        let noteController: NoteDetailsViewController = NoteDetailsViewController.instantiate()
        noteController.viewModel = NoteDetailsViewModel(folderId: folderId, database: dataBase)
        router.push(noteController)
    }
    
    func start(_ note: NoteProtocol) {
        let noteController: NoteDetailsViewController = NoteDetailsViewController.instantiate()
        noteController.viewModel = NoteDetailsViewModel(folderId: folderId, database: dataBase)
        noteController.viewModel.note = note
        router.push(noteController)
    }
}
