//
//  NoteDetailsViewModel.swift
//  Notes
//
//

import Foundation
import CoreData

class NoteDetailsViewModel {
    private let folderId: ObjectID
    private let database: DatabaseProtocol
    private lazy var creationDate: Date = { note?.creationDate ?? Date() }()
    
    var note: NoteProtocol?
    var creationDateTitle: String { creationDate.shortDate() }
    var body: String? { note?.body }
    
    init(folderId: ObjectID, database: DatabaseProtocol) {
        self.folderId = folderId
        self.database = database
    }
    
    func update(body: String) {
        guard !body.isEmpty else {
            return
        }
        
        let name = String(body.prefix(20))
        
        if let note = note {
            note.update(name: name, body: body)
        } else {
            database.create(noteName: name, body: body, folderId: folderId, creationDate: creationDate, completion: { _ in })
        }
    }
}
