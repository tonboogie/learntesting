//
//  AppCoordinator.swift
//  Notes
//
//

import UIKit

protocol Coordinator {
    func start()
}

class AppCoordinator: Coordinator {
    private let router: RouterProtocol
    private let dataBase: DatabaseProtocol = Database.shared
    
    init(router: RouterProtocol) {
        self.router = router
    }
    
    func start() {
        FolderListCoordinator(router: router, database: dataBase).start()
    }
}
