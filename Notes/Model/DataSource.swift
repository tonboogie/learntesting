//
//  DataSource.swift
//  Notes
//
//  Created by Anton Bugera on 27.10.2021.
//

import CoreData

// MARK: -
protocol DataSourceDelegate: NSFetchedResultsControllerDelegate {}
struct DataSourceSectionInfo {
    let info: NSFetchedResultsSectionInfo?
}

// MARK: -
protocol DataSourceProtocol: AnyObject {
    var delegate: DataSourceDelegate? { get set }
    var sections: [DataSourceSectionInfo]? { get }
    func performFetch() throws
    func object(at: IndexPath) -> Note
    func sort(by sort: SortCondition) throws
}

// MARK: - 
class DataSource: DataSourceProtocol {
    var delegate: DataSourceDelegate? {
        get {
            return fetchResultController.delegate as? DataSourceDelegate
        }
        set {
            fetchResultController.delegate = newValue
        }
    }
    
    var sections: [DataSourceSectionInfo]? {
        return fetchResultController.sections?.compactMap({ DataSourceSectionInfo(info: $0) })
    }
    
    private var folderId: NSManagedObjectID
    private var fetchResultController: NSFetchedResultsController<Note>
    
    init(sort: SortCondition, folderId: ObjectID) {
        guard let managedObjectID = folderId as? NSManagedObjectID else {
            fatalError()
        }
        self.folderId = managedObjectID
        let fetchRequest: NSFetchRequest<Note> = Note.fetchRequest()
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: sort.rawValue, ascending: true)]
        fetchRequest.predicate = NSPredicate(format: "folder == %@", managedObjectID)
        fetchResultController = NSFetchedResultsController(fetchRequest: fetchRequest,
                                             managedObjectContext: Database.shared.viewContext,
                                             sectionNameKeyPath: nil,
                                             cacheName: nil)
    }
    
    func performFetch() throws {
        try? fetchResultController.performFetch()
    }
    
    func object(at indexPath: IndexPath) -> Note {
        return fetchResultController.object(at: indexPath)
    }
    
    func sort(by sort: SortCondition) throws {
        let delegate = fetchResultController.delegate
        let fetchRequest: NSFetchRequest<Note> = Note.fetchRequest()
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: sort.rawValue, ascending: true)]
        fetchRequest.predicate = NSPredicate(format: "folder == %@", folderId)
        fetchResultController = NSFetchedResultsController(fetchRequest: fetchRequest,
                                             managedObjectContext: Database.shared.viewContext,
                                             sectionNameKeyPath: nil,
                                             cacheName: nil)
        fetchResultController.delegate = delegate
        try? performFetch()
    }
}
