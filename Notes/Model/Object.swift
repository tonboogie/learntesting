//
//  Object.swift
//  Notes
//
//  Created by Anton Bugera on 24.10.2021.
//

import CoreData

public protocol ObjectID: AnyObject {
    
}

extension NSManagedObjectID: ObjectID {
    
}
