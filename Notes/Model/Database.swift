//
//  Database.swift
//  Notes
//
//

import Foundation
import CoreData

protocol DatabaseProtocol {
    func open(completion: @escaping () -> Void)
    func create(folderName name: String, creationDate: Date, completion: @escaping (Error?) -> Void)
    func create(noteName name: String, body: String, folderId: ObjectID, creationDate: Date, completion: @escaping (Error?) -> Void)
    func delete(folder: FolderProtocol)
    func delete(note: NoteProtocol)
}

final class Database {
    static let shared = Database()
    
    let persistentContainer = NSPersistentContainer(name: "Notes")
    
    var viewContext: NSManagedObjectContext { persistentContainer.viewContext }
    
    

    private init() { }
}

extension Database: DatabaseProtocol {
    func open(completion: @escaping () -> Void) {
        persistentContainer.loadPersistentStores(completionHandler: { [weak self] _, error in
            self?.viewContext.automaticallyMergesChangesFromParent = true
            
            DispatchQueue.main.async(execute: completion)
        })
    }
    
    func create(folderName name: String, creationDate: Date, completion: @escaping (Error?) -> Void) {
        Folder.create(name: name, creationDate: creationDate, completion: completion )
    }
    
    func create(noteName name: String, body: String, folderId: ObjectID, creationDate: Date, completion: @escaping (Error?) -> Void) {
        Note.create(name: name, body: body, creationDate: creationDate, folderObjectId: folderId as! NSManagedObjectID)
    }
    
    func delete(folder: FolderProtocol) {
        guard let folder = folder as? Folder else {
            return
        }
        folder.delete()
    }
    
    func delete(note: NoteProtocol) {
        guard let note = note as? Note else {
            return
        }
        note.delete()
    }
}
